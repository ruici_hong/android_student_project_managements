# README #

This README would normally document whatever steps are necessary to get your application up and running.

* SDK version 9

### How it works ###

* 1: Student can create an entry for a new project. In order to create a new entry the student needs to provide some basic information, like Course Title, Course Number, Instructor Name, Project Number, project Description, Due data , etc.
User can create project by click “create project” button. Let's create 3 more projects.
![Screen Shot 2015-11-18 at 10.11.41 PM.png](https://bitbucket.org/repo/q4jA79/images/2761839687-Screen%20Shot%202015-11-18%20at%2010.11.41%20PM.png)

* 2: Sometimes student work in a group for the project. In addition, student(s) might have one or multiple tasks and corresponding task schedule for a particular project. Hence, the students might divide themselves over the different tasks of the project. In this application student will have the option to create one or multiple tasks for a project. Each task entry should include basic information like Task description, Task schedule . i.e., Date/Time, list of student names assigned for the task.
User can click “create tasks” button inside the project view above to create tasks. As we can see below. The created tasks will be shown on project interface.
![Screen Shot 2015-11-18 at 10.12.50 PM.png](https://bitbucket.org/repo/q4jA79/images/3303944241-Screen%20Shot%202015-11-18%20at%2010.12.50%20PM.png)

* 3: A student should be able to modify or delete any project or a task with a project.
User can long click the item on the project list or task list to delete it;
![Screen Shot 2015-11-18 at 10.13.47 PM.png](https://bitbucket.org/repo/q4jA79/images/3682110944-Screen%20Shot%202015-11-18%20at%2010.13.47%20PM.png)
In the project or task view user can also modify the detail by click the update button
![Screen Shot 2015-11-18 at 10.13.58 PM.png](https://bitbucket.org/repo/q4jA79/images/1269859853-Screen%20Shot%202015-11-18%20at%2010.13.58%20PM.png)

* 4: A student is able to track the completed project and completed tasks within each project.
Users can click the button view project/task by list of ”complete” to track them.
![Screen Shot 2015-11-18 at 10.15.19 PM.png](https://bitbucket.org/repo/q4jA79/images/3262559754-Screen%20Shot%202015-11-18%20at%2010.15.19%20PM.png)


* 5: The application should have an interface that allows the student to query on the project. For example, student should be able to display a report on the current status of any project (e.g. list of tasks as well as their due dates and whether a task is completed). Another example, student should get a summary report on the status of all uncompleted project stored.
Users can view the projects or tasks by “due date”, “priority”,”complete”,”incomplete”
![Screen Shot 2015-11-18 at 10.16.44 PM.png](https://bitbucket.org/repo/q4jA79/images/3929434555-Screen%20Shot%202015-11-18%20at%2010.16.44%20PM.png)

* 6: Once the application is launched, the students should get a warning list with all the projects/tasks that are within two days.
Once the application is launched, the list view will automatically switch to view by “warning list” which are due days within two days and status is “incomplete”.
![Screen Shot 2015-11-18 at 10.17.00 PM.png](https://bitbucket.org/repo/q4jA79/images/1995205574-Screen%20Shot%202015-11-18%20at%2010.17.00%20PM.png)

* 7 : The application should have an “About” button that should pop up your name and email.after click the about button;
![Screen Shot 2015-11-18 at 10.17.09 PM.png](https://bitbucket.org/repo/q4jA79/images/312771368-Screen%20Shot%202015-11-18%20at%2010.17.09%20PM.png)

* 8 : You can present the project or task in a sorted order based on due date. User can choose the view by “due date”
![Screen Shot 2015-11-18 at 10.19.33 PM.png](https://bitbucket.org/repo/q4jA79/images/2450374196-Screen%20Shot%202015-11-18%20at%2010.19.33%20PM.png)

* 9 : You can also allow the students to prioritize their projects or tasks in the application.
![Screen Shot 2015-11-18 at 10.19.44 PM.png](https://bitbucket.org/repo/q4jA79/images/3735276190-Screen%20Shot%202015-11-18%20at%2010.19.44%20PM.png)

* 10 : You can add background music.
Once the application launched, the background music will play.

* 11 :. You can allow exporting or importing your projects/tasks.
The sqlite database will can be export to the project local file, in this project will be will export to “com.han/files”, at beginning, the files is empty.
![Screen Shot 2015-11-18 at 10.21.26 PM.png](https://bitbucket.org/repo/q4jA79/images/473109052-Screen%20Shot%202015-11-18%20at%2010.21.26%20PM.png)

![Screen Shot 2015-11-18 at 10.21.37 PM.png](https://bitbucket.org/repo/q4jA79/images/2165755859-Screen%20Shot%202015-11-18%20at%2010.21.37%20PM.png)![Screen Shot 2015-11-18 at 10.21.49 PM.png](https://bitbucket.org/repo/q4jA79/images/1854940817-Screen%20Shot%202015-11-18%20at%2010.21.49%20PM.png)![Screen Shot 2015-11-18 at 10.21.59 PM.png](https://bitbucket.org/repo/q4jA79/images/2265890347-Screen%20Shot%202015-11-18%20at%2010.21.59%20PM.png)