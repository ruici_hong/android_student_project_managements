/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package com.han;

public final class R {
    public static final class attr {
    }
    public static final class drawable {
        public static final int arrow=0x7f020000;
        public static final int ic_launcher=0x7f020001;
    }
    public static final class id {
        public static final int Priority=0x7f070017;
        public static final int Status=0x7f070015;
        public static final int arrow=0x7f070019;
        public static final int btn_about=0x7f070011;
        public static final int btn_confirm=0x7f070007;
        public static final int btn_create_project=0x7f07000f;
        public static final int btn_export=0x7f070012;
        public static final int btn_import=0x7f070013;
        public static final int btn_tasks=0x7f07000c;
        public static final int btn_update=0x7f070008;
        public static final int complete=0x7f07000b;
        public static final int description=0x7f07001b;
        public static final int due_date=0x7f070016;
        public static final int et_Instructor_Name=0x7f070002;
        public static final int et_course_number=0x7f070001;
        public static final int et_due_date=0x7f070006;
        public static final int et_participants=0x7f07001d;
        public static final int et_project_des=0x7f070004;
        public static final int et_project_number=0x7f070003;
        public static final int et_schedule=0x7f07001e;
        public static final int et_title=0x7f070000;
        public static final int incomplete=0x7f07000a;
        public static final int list_view=0x7f07000e;
        public static final int myRadioGroup=0x7f070009;
        public static final int progress_bar=0x7f07001a;
        public static final int pull_to_refresh_head=0x7f070018;
        public static final int spinner=0x7f070005;
        public static final int title=0x7f070014;
        public static final int tv=0x7f070010;
        public static final int updated_at=0x7f07001c;
        public static final int view_query=0x7f07000d;
    }
    public static final class layout {
        public static final int activity_edit_project=0x7f030000;
        public static final int main=0x7f030001;
        public static final int project_row=0x7f030002;
        public static final int pull_to_refresh=0x7f030003;
        public static final int task=0x7f030004;
    }
    public static final class raw {
        public static final int www=0x7f040000;
    }
    public static final class string {
        public static final int app_name=0x7f050000;
        public static final int body=0x7f050006;
        public static final int confirm=0x7f05000c;
        public static final int due_date=0x7f05000b;
        public static final int edit_note=0x7f05000d;
        public static final int hello_world=0x7f05000f;
        public static final int menu_delete=0x7f050003;
        public static final int menu_insert=0x7f050002;
        public static final int menu_view_map=0x7f050004;
        public static final int name=0x7f050008;
        public static final int no_projects=0x7f050001;
        public static final int not_updated_yet=0x7f050016;
        public static final int number=0x7f050007;
        public static final int project_des=0x7f05000a;
        public static final int project_number=0x7f050009;
        public static final int pull_to_refresh=0x7f050010;
        public static final int refreshing=0x7f050012;
        public static final int release_to_refresh=0x7f050011;
        public static final int time_error=0x7f050015;
        public static final int title=0x7f050005;
        public static final int title_activity_edit_note=0x7f05000e;
        public static final int title_activity_map=0x7f050017;
        public static final int updated_at=0x7f050013;
        public static final int updated_just_now=0x7f050014;
    }
    public static final class style {
        /** 
        Base application theme, dependent on API level. This theme is replaced
        by AppBaseTheme from res/values-vXX/styles.xml on newer devices.
    

            Theme customizations available in newer API levels can go in
            res/values-vXX/styles.xml, while customizations related to
            backward-compatibility can go here.
        
         */
        public static final int AppBaseTheme=0x7f060000;
        /**  Application theme. 
 All customizations that are NOT specific to a particular API-level can go here. 
         */
        public static final int AppTheme=0x7f060001;
    }
}
