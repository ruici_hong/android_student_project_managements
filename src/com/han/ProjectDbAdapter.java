/*
 * Copyright (C) 2008 Google Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.han;

import java.util.Date;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Simple notes database access helper class. Defines the basic CRUD operations
 * for the notepad example, and gives the ability to list all notes as well as
 * retrieve or modify a specific note.
 * 
 * This has been improved from the first version of this tutorial through the
 * addition of better error handling and also using returning a Cursor instead
 * of using a collection of inner classes (which is less scalable and not
 * recommended).
 */
public class ProjectDbAdapter {

    public static final String KEY_TITLE = "title";
    public static final String KEY_BODY = "body";
    public static final String KEY_ROWID = "_id";
    public static final String COURSE_NUMBER = "course_number";
	public static final String INSTRUCTOR_NAME ="instructor_name";
	public static final String PROJECT_NUMBER = "project_number";
	public static final String DUE_DATE = "due_date";
    private static final String TAG = "ProjectDbAdapter";
	public static final String PARTICIPANTS = "participants";
	public static final String SCHEDULE = "schedule";
	public static final String PROJECTID = "projectid";
	public static final String STATUS = "status";
	public static final String PRIORITY = "priority";
    private DatabaseHelper mDbHelper;
    private SQLiteDatabase mDb;

    /**
     * Database creation sql statement
     */
    private static final String DATABASE_CREATE =
        "CREATE TABLE IF NOT EXISTS projects (_id integer primary key autoincrement, "
        + "title text not null, "
        + "body text not null,"
        + "course_number text not null,"
        + "instructor_name text not null,"
        + "project_number text not null,"
        + "due_date date not null,"
        + "status text not null,"
        + "priority integer not null"
        + ");";
    private static final String DATABASE_TASK_CREATE =
            "CREATE TABLE IF NOT EXISTS  tasks (_id integer primary key autoincrement,"
            + "title text not null, "
            + "body text not null,"
            + "participants text not null,"
            + "schedule text not null,"
            +" status text not null,"
            +" projectid integer not null,"
            + "priority integer not null,"
            + "foreign key (projectid)  references projects(_id) on delete cascade"
            + ");";
    
    
    private static final String DATABASE_NAME = "data";
    private static final String DATABASE_TABLE = "projects";
    private static final int DATABASE_VERSION = 2;

    private final Context mCtx;

    private static class DatabaseHelper extends SQLiteOpenHelper {

        DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
        	db.execSQL("PRAGMA foreign_keys=ON;");
            db.execSQL(DATABASE_CREATE);
            db.execSQL(DATABASE_TASK_CREATE);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            Log.w(TAG, "Upgrading database from version " + oldVersion + " to "
                    + newVersion + ", which will destroy all old data");
            db.execSQL("DROP TABLE IF EXISTS projects");
            db.execSQL("DROP TABLE IF EXISTS tasks");
            onCreate(db);
        }
        
        
    }

    /**
     * Constructor - takes the context to allow the database to be
     * opened/created
     * 
     * @param ctx the Context within which to work
     */
    public ProjectDbAdapter(Context ctx) {
        this.mCtx = ctx;
    }

    /**
     * Open the notes database. If it cannot be opened, try to create a new
     * instance of the database. If it cannot be created, throw an exception to
     * signal the failure
     * 
     * @return this (self reference, allowing this to be chained in an
     *         initialization call)
     * @throws SQLException if the database could be neither opened or created
     */
    public ProjectDbAdapter open() throws SQLException {
        mDbHelper = new DatabaseHelper(mCtx);
        mDb = mDbHelper.getWritableDatabase();
        mDb.execSQL("PRAGMA foreign_keys = ON;");
        return this;
    }

    public void close() {
        mDbHelper.close();
    }


    /**
     * Create a new note using the title and body provided. If the note is
     * successfully created return the new rowId for that note, otherwise return
     * a -1 to indicate failure.
     * 
     * @param title the title of the note
     * @param body the body of the note
     * @return rowId or -1 if failed
     */
    public long createProject(String title, String body,String courseNumber, String instructorName,
    		String projectNumber, String due_date,String status,int priority) {
        ContentValues initialValues = new ContentValues();
        initialValues.put(KEY_TITLE, title);
        initialValues.put(KEY_BODY, body);
        initialValues.put(COURSE_NUMBER, courseNumber);
        initialValues.put(INSTRUCTOR_NAME, instructorName);
        initialValues.put(PROJECT_NUMBER, projectNumber);
        initialValues.put(DUE_DATE, due_date);
        initialValues.put(STATUS, status);
        initialValues.put(PRIORITY, priority);
        return mDb.insert(DATABASE_TABLE, null, initialValues);
    }
    
    public long createTasks(String title, String body,String participants, String schedule,
    		long projectid,String status,int priority) {
        ContentValues initialValues = new ContentValues();
        initialValues.put(KEY_TITLE, title);
        initialValues.put(KEY_BODY, body);
        initialValues.put(PARTICIPANTS, participants);
        initialValues.put(SCHEDULE, schedule);
        initialValues.put(PROJECTID, projectid);
        initialValues.put(STATUS, status);
        initialValues.put(PRIORITY, priority);
        return mDb.insert("tasks", null, initialValues);
    }

    /**
     * Delete the note with the given rowId
     * 
     * @param rowId id of note to delete
     * @return true if deleted, false otherwise
     */
    public boolean deleteNote(long rowId) {

        return mDb.delete(DATABASE_TABLE, KEY_ROWID + "=" + rowId, null) > 0;
    }
    public boolean deleteTask(long rowId) {

        return mDb.delete("tasks", KEY_ROWID + "=" + rowId, null) > 0;
    }
    /**
     * Return a Cursor over the list of all notes in the database
     * 
     * @return Cursor over all notes
     */
    public Cursor fetchAllProjects() {
        return mDb.query(DATABASE_TABLE, new String[] {KEY_ROWID, KEY_TITLE,
                KEY_BODY,COURSE_NUMBER,INSTRUCTOR_NAME,PROJECT_NUMBER,STATUS,DUE_DATE,PRIORITY}, null, null, null, null, null);
    }
    
    public Cursor fetchTasksByStatus(String status, Long projectid) throws SQLException {

        Cursor mCursor =
            mDb.query(true, "tasks", new String[] {KEY_ROWID, 
                    KEY_TITLE, KEY_BODY,PARTICIPANTS,SCHEDULE,PROJECTID,STATUS,PRIORITY}, 
                    STATUS + "='" +status+"' and "+PROJECTID+"="+projectid, null,
                    null, null, null, null);
        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;

    }
    public Cursor fetchTasksByOrder(String order,Long projectid) throws SQLException {

        Cursor mCursor =
            mDb.query(true, "tasks", new String[] {KEY_ROWID, 
                    KEY_TITLE, KEY_BODY,PARTICIPANTS,SCHEDULE,PROJECTID,STATUS,PRIORITY}, 
                    PROJECTID+"="+projectid, null,
                    null, null, order, null);
        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;

    }
    
    public Cursor fetchAllTasks() {
        return mDb.query("tasks", new String[] {KEY_ROWID,KEY_TITLE,
                KEY_BODY,PARTICIPANTS,SCHEDULE,PROJECTID,STATUS,PRIORITY}, null, null, null, null, null);
    }
    public Cursor fetchTasks(long rowId) throws SQLException {

        Cursor mCursor =

            mDb.query(true, "tasks", new String[] {KEY_ROWID, 
                    KEY_TITLE, KEY_BODY,PARTICIPANTS,SCHEDULE,PROJECTID,STATUS,PRIORITY}, 
                    KEY_ROWID + "=" + rowId, null,
                    null, null, null, null);
        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;

    }
    public Cursor fetchTasksByProjectId(long rowId) throws SQLException {

        Cursor mCursor =

            mDb.query(true, "tasks", new String[] {KEY_ROWID, 
                    KEY_TITLE, KEY_BODY,PARTICIPANTS,SCHEDULE,PROJECTID,STATUS,PRIORITY}, 
                    PROJECTID + "=" + rowId, null,
                    null, null, null, null);
        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;

    }
    /**
     * Return a Cursor positioned at the note that matches the given rowId
     * 
     * @param rowId id of note to retrieve
     * @return Cursor positioned to matching note, if found
     * @throws SQLException if note could not be found/retrieved
     */
    public Cursor fetchProject(long rowId) throws SQLException {

        Cursor mCursor =

            mDb.query(true, DATABASE_TABLE, new String[] {KEY_ROWID,
                    KEY_TITLE, KEY_BODY,COURSE_NUMBER,INSTRUCTOR_NAME,PROJECT_NUMBER,STATUS,DUE_DATE,PRIORITY}, 
                    KEY_ROWID + "=" + rowId, null,
                    null, null, null, null);
        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;

    }
    public Cursor fetchProjectByStatus(String status) throws SQLException {

        Cursor mCursor =

            mDb.query(true, DATABASE_TABLE, new String[] {KEY_ROWID,
                    KEY_TITLE, KEY_BODY,COURSE_NUMBER,INSTRUCTOR_NAME,PROJECT_NUMBER,STATUS,DUE_DATE,PRIORITY}, 
                    STATUS + "='" +status+ "'", null,
                    null, null, null, null);
        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;

    }
    
    public Cursor fetchProjectByDueDate(String duedate) throws SQLException {

        Cursor mCursor =

            mDb.query(true, DATABASE_TABLE, new String[] {KEY_ROWID,
                    KEY_TITLE, KEY_BODY,COURSE_NUMBER,INSTRUCTOR_NAME,PROJECT_NUMBER,STATUS,DUE_DATE,PRIORITY}, 
                    DUE_DATE + ">='" +duedate+ "' and "+STATUS+"='incomplete'", null,
                    null, null, null, null);
        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;

    }
    
    public Cursor fetchProjectByOrder(String order) throws SQLException {

        Cursor mCursor =

            mDb.query(true, DATABASE_TABLE, new String[] {KEY_ROWID,
                    KEY_TITLE, KEY_BODY,COURSE_NUMBER,INSTRUCTOR_NAME,PROJECT_NUMBER,STATUS,DUE_DATE,PRIORITY}, 
                    null, null,
                    null, null, order, null);
        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;

    }

    /**
     * Update the note using the details provided. The note to be updated is
     * specified using the rowId, and it is altered to use the title and body
     * values passed in
     * 
     * @param rowId id of note to update
     * @param title value to set note title to
     * @param body value to set note body to
     * @return true if the note was successfully updated, false otherwise
     */
    public boolean updateProject(long rowId, String title, String body,String courseNumber, String instructorName,
    		String projectNumber, String due_date,String status,int priority) {
        ContentValues args = new ContentValues();
        args.put(KEY_TITLE, title);
        args.put(KEY_BODY, body);
        args.put(COURSE_NUMBER, courseNumber);
        args.put(INSTRUCTOR_NAME, instructorName);
        args.put(PROJECT_NUMBER, projectNumber);
        args.put(DUE_DATE, due_date);
        args.put(STATUS, status);
        args.put(PRIORITY, priority);

        return mDb.update(DATABASE_TABLE, args, KEY_ROWID + "=" + rowId, null) > 0;
    }
    
    public boolean updateTasks(long rowId, String title, String body,String participants, String schedule,
    		long projectid,String status,int priority) {
        ContentValues args = new ContentValues();
        args.put(KEY_TITLE, title);
        args.put(KEY_BODY, body);
        args.put(PARTICIPANTS, participants);
        args.put(SCHEDULE, schedule);
        args.put(STATUS, status);
        args.put(PRIORITY, priority);

        return mDb.update("tasks", args, KEY_ROWID + "=" + rowId, null) > 0;
    }
}
