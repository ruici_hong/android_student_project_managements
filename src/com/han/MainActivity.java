package com.han;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;

import android.app.ListActivity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.AdapterView.AdapterContextMenuInfo;

public class MainActivity extends ListActivity implements OnClickListener {
	private Button btn_create_project,btn_about,btn_export,btn_import;
	private ProjectDbAdapter mDbHelper;
    private static final int DELETE_ID = Menu.FIRST + 1;
    private static final int ACTIVITY_EDIT = 2;
	private Spinner view_query;
	private ArrayAdapter query;
	private static boolean first_time = true;
	private String[] allQueryValues = {"Complete","Incomplete","Due Date","Priority","All","Warning list"};
	private Intent intent = new Intent("com.angel.Android.MUSIC");
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.main);
	    mDbHelper = new ProjectDbAdapter(this);
	    startService(intent);

		//asdada



        //xiao fei zhu

		btn_create_project = (Button)findViewById(R.id.btn_create_project);
		btn_create_project.setOnClickListener(this);
		btn_about = (Button)findViewById(R.id.btn_about);
		btn_about.setOnClickListener(this);
		btn_export = (Button)findViewById(R.id.btn_export);
		btn_export.setOnClickListener(this);
		btn_import = (Button)findViewById(R.id.btn_import);
		btn_import.setOnClickListener(this);
		this.registerForContextMenu(getListView());  
		view_query  = (Spinner) findViewById(R.id.view_query);
		query = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, allQueryValues);
		query.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		view_query.setAdapter(query);
		
		view_query.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				switch (position){
				case 0: //Complete
					mDbHelper.open();
					fillData(mDbHelper.fetchProjectByStatus("complete"));
					mDbHelper.close();
					break;
				case 1: //inComplete
					mDbHelper.open();
					fillData(mDbHelper.fetchProjectByStatus("incomplete"));
					mDbHelper.close();
					break;
				case 2: //due
					mDbHelper.open();
					fillData(mDbHelper.fetchProjectByOrder("due_date"));
					mDbHelper.close();
					break;
				case 3: //priority
					mDbHelper.open();
					fillData(mDbHelper.fetchProjectByOrder("priority DESC"));
					mDbHelper.close();
					break;
				case 4: //all
					mDbHelper.open();
					fillData(mDbHelper.fetchAllProjects());
					mDbHelper.close();
					break;
				case 5: //warning list
					mDbHelper.open();
					SimpleDateFormat sDateFormat = new SimpleDateFormat("yyyMMdd");     
					String date = sDateFormat.format(new java.util.Date());  				
					fillData(mDbHelper.fetchProjectByDueDate(date));					
					mDbHelper.close();
					break;
					}
				
				
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				
			}
		});


	}
	
	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.btn_create_project:
			Intent mIntent = new Intent(MainActivity.this, ProjectActivity.class);
			startActivityForResult(mIntent, 1);
			break;
		case R.id.btn_about:
			Toast.makeText(MainActivity.this, "name: youjiahan   "
					+ "email: yhan069@uottawa.ca", Toast.LENGTH_LONG).show();
			break;
		case R.id.btn_export:		
	       File data = Environment.getDataDirectory();
	       FileChannel source=null;
	       FileChannel destination=null;
	       String currentDBPath = "/data/"+ "com.han" +"/databases/data";
	       String backupDBPath = "/data/"+ "com.han" +"/files/data";
	       File currentDB = new File(data, currentDBPath);
	       File backupDB = new File(data, backupDBPath);
	       
	       try {
	            source = new FileInputStream(currentDB).getChannel();
	            destination = new FileOutputStream(backupDB).getChannel();
	            destination.transferFrom(source, 0, source.size());
	            source.close();
	            destination.close();
	            Toast.makeText(this, "DB Exported!", Toast.LENGTH_LONG).show();
	        } catch(IOException e) {
	        	e.printStackTrace();
	        }
			break;
		case R.id.btn_import:
		      File data1 = Environment.getDataDirectory();
		       FileChannel source1=null;
		       FileChannel destination1=null;
		       String currentDBPath1 = "/data/"+ "com.han" +"/databases/data";
		       String backupDBPath1 = "/data/"+ "com.han" +"/files/data";
		       File currentDB1 = new File(data1, currentDBPath1);
		       File backupDB1 = new File(data1, backupDBPath1);
		       
		       try {
		            source = new FileInputStream(backupDB1).getChannel();
		            destination = new FileOutputStream(currentDB1).getChannel();
		            destination.transferFrom(source, 0, source.size());
		            source.close();
		            destination.close();
		            Toast.makeText(this, "DB Imported!", Toast.LENGTH_LONG).show();
		        } catch(IOException e) {
		        	e.printStackTrace();
		        Toast.makeText(this, "DB Imported fail, please export file first!", Toast.LENGTH_LONG).show();
		        }
			
			break;
		default: 
		  break;					
		}		
	}
	
    private void fillData(Cursor projectsCursor) {

        startManagingCursor(projectsCursor);

        // Create an array to specify the fields we want to display in the list (only TITLE)
        String[] from = new String[]{mDbHelper.KEY_TITLE,mDbHelper.STATUS,mDbHelper.DUE_DATE,mDbHelper.PRIORITY};

        // and an array of the fields we want to bind those fields to (in this case just text1)
        int[] to = new int[]{R.id.title,R.id.Status,R.id.due_date,R.id.Priority};

        // Now create a simple cursor adapter and set it to display
        SimpleCursorAdapter projects = 
            new SimpleCursorAdapter(this, R.layout.project_row, projectsCursor, from, to);
        setListAdapter(projects);
     
    }
	

    
    
    
    @Override
    protected void onResume() {
    	super.onResume();
    	if(first_time){
    		mDbHelper.open();
    		SimpleDateFormat sDateFormat = new SimpleDateFormat("yyyMMdd");     
			String date = sDateFormat.format(new java.util.Date());  				
			fillData(mDbHelper.fetchProjectByDueDate(date));	
			first_time = false;
			view_query.setSelection(5);
			mDbHelper.close();
    	}else{
    		view_query.setSelection(4);
        	mDbHelper.open();
    		fillData(mDbHelper.fetchAllProjects());
    		mDbHelper.close();
    	}
    
    }
    
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
            ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.add(0, DELETE_ID, 0, "delete project");
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case DELETE_ID:
            	mDbHelper.open();
                AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
                mDbHelper.deleteNote(info.id);
              
                fillData(mDbHelper.fetchAllProjects());
                mDbHelper.close();
                return true;
        }
        return super.onContextItemSelected(item);
    }
    



	@Override
	   protected void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
        Intent i = new Intent(this, ProjectActivity.class);
        i.putExtra(mDbHelper.KEY_ROWID, id);
        startActivityForResult(i, ACTIVITY_EDIT);
		
	}
}
