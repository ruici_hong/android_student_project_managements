package com.han;

import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

public class TaskActivity extends Activity{
	private Spinner spinner;
	private ArrayAdapter adapter;
	private String[] allAreaValues = {"1","2","3","4","5"};
	private EditText et_title,et_participants,et_project_des;
	private DatePicker et_schedule;
	private RadioGroup myRadioGroup;
	private Button btn_confirm,btn_update;
	private ProjectDbAdapter mDbHelper;
	private Long mRowId ;
	private Long projectID;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.task);
	    mDbHelper = new ProjectDbAdapter(this);
	 
			Bundle extras = getIntent().getExtras();
			mRowId = extras.getLong(mDbHelper.KEY_ROWID);
	
			projectID = extras.getLong(mDbHelper.PROJECTID);
									
		spinner = (Spinner) findViewById(R.id.spinner);
		adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, allAreaValues);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner.setAdapter(adapter);
        System.out.println("KEY_ROWID"+mRowId +"PROJECTID"+projectID);
		init();
		if (mRowId != null&&mRowId !=0) {
			btn_confirm.setVisibility(8);
			btn_update.setVisibility(0);
		}
		else{
			btn_confirm.setVisibility(0);
			btn_update.setVisibility(8);
		}
		populateFields();
	}
	
	   private void populateFields() {
		   System.out.println("mRowId"+mRowId);
	        if (mRowId != null&&mRowId !=0) {
	        	mDbHelper.open();
	            Cursor project = mDbHelper.fetchTasks(mRowId);
	            startManagingCursor(project);
	            et_title.setText(project.getString(
	            		project.getColumnIndexOrThrow(mDbHelper.KEY_TITLE)));
	            et_project_des.setText(project.getString(
	            		project.getColumnIndexOrThrow(mDbHelper.KEY_BODY)));
	            et_participants.setText(project.getString(
	            		project.getColumnIndexOrThrow(mDbHelper.PARTICIPANTS)));
	            String date = project.getString(
	            		project.getColumnIndexOrThrow(mDbHelper.SCHEDULE));
	            String year = date.substring(0, 4);
	            String month = date.substring(5, 6);
	            String day = date.substring(6, 8);
	     
	            et_schedule.updateDate(Integer.valueOf(year), Integer.valueOf(month), Integer.valueOf(day));
	            String id = project.getString(project.getColumnIndexOrThrow(mDbHelper.STATUS));
	            String priority = project.getString(project.getColumnIndexOrThrow(mDbHelper.PRIORITY));
	            spinner.setSelection(Integer.valueOf(priority)-1);
	            if(id.equals("complete"))
            	myRadioGroup.check(R.id.complete);
	            mDbHelper.close();
	        
	        }
	    }
	
	
	public void init(){
	
		
		et_title = (EditText)findViewById(R.id.et_title);
		et_participants = (EditText)findViewById(R.id.et_participants);
		et_project_des = (EditText)findViewById(R.id.et_project_des);
		et_schedule  =(DatePicker)findViewById(R.id.et_schedule);
		myRadioGroup = (RadioGroup)findViewById(R.id.myRadioGroup);
		btn_confirm = (Button)findViewById(R.id.btn_confirm);
		btn_confirm.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(TextUtils.isEmpty(et_title.getText())){
					Toast.makeText(TaskActivity.this, "title can not null", Toast.LENGTH_SHORT).show();
					return ;
				}
				if(TextUtils.isEmpty(et_project_des.getText())){
					Toast.makeText(TaskActivity.this, "project description can not null", Toast.LENGTH_SHORT).show();
					return ;
				}
				if(TextUtils.isEmpty(et_participants.getText())){
					Toast.makeText(TaskActivity.this, "participants description can not null", Toast.LENGTH_SHORT).show();
					return ;
				}
				int year = et_schedule.getYear();
				String month = et_schedule.getMonth()+"";
				if(et_schedule.getMonth()<10)
					month ="0"+month;
				String day = et_schedule.getDayOfMonth()+"";
				if(et_schedule.getDayOfMonth()<10)
					day ="0"+day;
				String time = year+""+month+""+day+"";
				int radioId = myRadioGroup.getCheckedRadioButtonId();
				String status = radioId == R.id.complete ? "complete" : "incomplete";
				int priority = Integer.valueOf( (String) spinner.getSelectedItem());
				mDbHelper.open();
			
			long id = mDbHelper.createTasks(et_title.getText().toString(), et_project_des.getText().toString(), 
					et_participants.getText().toString(), time, projectID,status,priority);
				mDbHelper.close();	
				if(id>0){
					Toast.makeText(TaskActivity.this, "task insert successfully", Toast.LENGTH_SHORT).show();
					TaskActivity.this.setResult(1);
					TaskActivity.this.finish();
				}
				else{
			    Toast.makeText(TaskActivity.this, "task insert fail", Toast.LENGTH_SHORT).show();				
				}
			}
		});	
		btn_update = (Button)findViewById(R.id.btn_update);
		btn_update.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(TextUtils.isEmpty(et_title.getText())){
					Toast.makeText(TaskActivity.this, "title can not null", Toast.LENGTH_SHORT).show();
					return ;
				}
				if(TextUtils.isEmpty(et_project_des.getText())){
					Toast.makeText(TaskActivity.this, "project description can not null", Toast.LENGTH_SHORT).show();
					return ;
				}
				if(TextUtils.isEmpty(et_participants.getText())){
					Toast.makeText(TaskActivity.this, "participants description can not null", Toast.LENGTH_SHORT).show();
					return ;
				}
				int year = et_schedule.getYear();
				String month = et_schedule.getMonth()+"";
				if(et_schedule.getMonth()<10)
					month ="0"+month;
				String day = et_schedule.getDayOfMonth()+1+"";
				if(et_schedule.getDayOfMonth()<10)
					day ="0"+day;
				String time = year+""+month+""+day+"";
				int radioId = myRadioGroup.getCheckedRadioButtonId();
				String status = radioId == R.id.complete ? "complete" : "incomplete";
				int priority = Integer.valueOf( (String) spinner.getSelectedItem());
				mDbHelper.open();
			
			boolean result = mDbHelper.updateTasks(mRowId,et_title.getText().toString(), et_project_des.getText().toString(), 
					et_participants.getText().toString(), time, projectID,status,priority);
				mDbHelper.close();	
				if(result){
					Toast.makeText(TaskActivity.this, "task update successfully", Toast.LENGTH_SHORT).show();
					TaskActivity.this.setResult(1);
					TaskActivity.this.finish();
				}
				else{
			    Toast.makeText(TaskActivity.this, "task update fail", Toast.LENGTH_SHORT).show();				
				}
				
				
				
			}
		});
		
	}
	
	
	
	
}
