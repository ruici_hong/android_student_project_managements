package com.han;





import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.SimpleCursorAdapter;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.AdapterView.AdapterContextMenuInfo;



public class ProjectActivity extends Activity implements  OnClickListener, OnItemClickListener {
	private Spinner spinner,view_query;
	private ArrayAdapter adapter;
	private ArrayAdapter query;
	private String[] allAreaValues = {"1","2","3","4","5"};
	private String[] allQueryValues = {"Complete","Incomplete","Due Date","Priority","All"};
	private EditText et_title;
	private EditText et_course_number;
	private EditText et_Instructor_Name;
	private EditText et_project_number;
	private EditText et_project_des;
	private DatePicker et_due_date;
	private Button btn_confirm,btn_update;
	private Button btn_tasks;
	private ProjectDbAdapter mDbHelper;
	private Long mRowId ;
	private RadioGroup rg;
	private ListView list_view;
	private static final int ACTIVITY_CREATE = 1;
	private static final int ACTIVITY_EDIT = 2;
	private static final int DELETE_ID = Menu.FIRST + 1;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_edit_project);
	    mDbHelper = new ProjectDbAdapter(this);
	
        mRowId = (savedInstanceState == null) ? null :
            (Long) savedInstanceState.getSerializable(mDbHelper.KEY_ROWID);
        
		if (mRowId == null) {
			Bundle extras = getIntent().getExtras();
			mRowId = extras != null ? extras.getLong(mDbHelper.KEY_ROWID)
									: null;
		}
	  
		init();
		this.registerForContextMenu(list_view);
		populateFields();
		
	}
	
	private void fillData(Cursor tasksCursor) {
		if (mRowId != null) {
	
        startManagingCursor(tasksCursor);

        // Create an array to specify the fields we want to display in the list (only TITLE)
        String[] from = new String[]{mDbHelper.KEY_TITLE,mDbHelper.STATUS,mDbHelper.SCHEDULE,mDbHelper.PRIORITY};

        // and an array of the fields we want to bind those fields to (in this case just text1)
        int[] to = new int[]{R.id.title,R.id.Status,R.id.due_date,R.id.Priority};

        // Now create a simple cursor adapter and set it to display
        SimpleCursorAdapter tasks = 
            new SimpleCursorAdapter(this, R.layout.project_row, tasksCursor, from, to);
        list_view.setAdapter(tasks);
        list_view.setOnItemClickListener(this);
  
		}
    }
	
	
    private void populateFields() {
        if (mRowId != null) {
        	mDbHelper.open();
            Cursor project = mDbHelper.fetchProject(mRowId);
            startManagingCursor(project);
            et_title.setText(project.getString(
            		project.getColumnIndexOrThrow(mDbHelper.KEY_TITLE)));
            et_project_des.setText(project.getString(
            		project.getColumnIndexOrThrow(mDbHelper.KEY_BODY)));
            et_course_number.setText(project.getString(
            		project.getColumnIndexOrThrow(mDbHelper.COURSE_NUMBER)));
            et_Instructor_Name.setText(project.getString(
            		project.getColumnIndexOrThrow(mDbHelper.INSTRUCTOR_NAME)));
            et_project_number.setText(project.getString(
            		project.getColumnIndexOrThrow(mDbHelper.PROJECT_NUMBER)));
            String date = project.getString(
            		project.getColumnIndexOrThrow(mDbHelper.DUE_DATE));
            String year = date.substring(0, 4);
            String month = date.substring(5, 6);
            String day = date.substring(6, 8);
     
            et_due_date.updateDate(Integer.valueOf(year), Integer.valueOf(month), Integer.valueOf(day));
            String id = project.getString(project.getColumnIndexOrThrow(mDbHelper.STATUS));
            String priority = project.getString(project.getColumnIndexOrThrow(mDbHelper.PRIORITY));
            spinner.setSelection(Integer.valueOf(priority)-1);
            if(id.equals("complete"))
            rg.check(R.id.complete);
            mDbHelper.close();
        
        }
    }
    
	public void init(){
		et_title = (EditText)findViewById(R.id.et_title);
		et_project_des = (EditText)findViewById(R.id.et_project_des);
		et_course_number = (EditText)findViewById(R.id.et_course_number);
		et_Instructor_Name = (EditText)findViewById(R.id.et_Instructor_Name);
		et_project_number = (EditText)findViewById(R.id.et_project_number);
		et_due_date = (DatePicker)findViewById(R.id.et_due_date);
		btn_confirm = (Button)findViewById(R.id.btn_confirm);
		btn_confirm.setOnClickListener(this);
		btn_update = (Button)findViewById(R.id.btn_update);
		btn_update.setOnClickListener(this);
		btn_tasks = (Button)findViewById(R.id.btn_tasks);
		btn_tasks.setOnClickListener(this);		
	

		
		rg  = (RadioGroup)findViewById(R.id.myRadioGroup);
		list_view = (ListView)findViewById(R.id.list_view);
		spinner = (Spinner) findViewById(R.id.spinner);
		view_query  = (Spinner) findViewById(R.id.view_query);
		
		query = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, allQueryValues);
		query.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		view_query.setAdapter(query);

		view_query.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				System.out.println("position"+position);
				switch (position){
				case 0: //Complete
					mDbHelper.open();
					fillData(mDbHelper.fetchTasksByStatus("complete",mRowId));
					mDbHelper.close();
					break;
				case 1: //inComplete
					mDbHelper.open();
					fillData(mDbHelper.fetchTasksByStatus("incomplete",mRowId));
					mDbHelper.close();
					break;
				case 2: //due
					mDbHelper.open();
					fillData(mDbHelper.fetchTasksByOrder("schedule",mRowId));
					mDbHelper.close();
					break;
				case 3:
					mDbHelper.open();
					fillData(mDbHelper.fetchTasksByOrder("priority DESC",mRowId));
					mDbHelper.close();
					break;
				case 4: //all
					mDbHelper.open();
					fillData(mDbHelper.fetchTasksByProjectId(mRowId));
					mDbHelper.close();
					break;
					
					
					}
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				
			}
		});
		
		
		adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, allAreaValues);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner.setAdapter(adapter);
		
		if(mRowId != null){
			btn_confirm.setVisibility(8);
			btn_update.setVisibility(0);
		}
		else{
			btn_confirm.setVisibility(0);
			btn_update.setVisibility(8);
		}
		
		
	}

	@Override
	public void onClick(View v) {
		
		switch(v.getId()){
		case R.id.btn_confirm:		
			if(TextUtils.isEmpty(et_title.getText())){
				Toast.makeText(ProjectActivity.this, "title can not null", Toast.LENGTH_SHORT).show();
				return ;
			}
			if(TextUtils.isEmpty(et_project_des.getText())){
				Toast.makeText(ProjectActivity.this, "project description can not null", Toast.LENGTH_SHORT).show();
				return ;
			}if(TextUtils.isEmpty(et_course_number.getText())){
				Toast.makeText(ProjectActivity.this, "course number can not null", Toast.LENGTH_SHORT).show();
				return ;
			}if(TextUtils.isEmpty(et_Instructor_Name.getText())){
				Toast.makeText(ProjectActivity.this, "instructor name can not null", Toast.LENGTH_SHORT).show();
				return ;
			}if(TextUtils.isEmpty(et_project_number.getText())){
				Toast.makeText(ProjectActivity.this, "project number can not null", Toast.LENGTH_SHORT).show();
				return ;
			}
			int year = et_due_date.getYear();
			String month = et_due_date.getMonth()+1+"";
			if(et_due_date.getMonth()<10)
				month ="0"+month;
			String day = et_due_date.getDayOfMonth()+"";
			if(et_due_date.getDayOfMonth()<10)
				day ="0"+day;
			String time = year+""+month+""+day+"";
			int priority = Integer.valueOf( (String) spinner.getSelectedItem());
			int radioId = rg.getCheckedRadioButtonId();
			String status = radioId == R.id.complete ? "complete" : "incomplete";
			mDbHelper.open();
		
			 mRowId = mDbHelper.createProject(et_title.getText().toString(), et_project_des.getText().toString(), 
					et_course_number.getText().toString(), et_Instructor_Name.getText().toString(),
					et_project_number.getText().toString(), time,status,priority);
			if(mRowId>0){
			Toast.makeText(ProjectActivity.this, "project insert successfully", Toast.LENGTH_SHORT).show();
			ProjectActivity.this.finish();
			}
			else{
		    Toast.makeText(ProjectActivity.this, "project insert fail", Toast.LENGTH_SHORT).show();				
			}
			mDbHelper.close();	
		break;
		case R.id.btn_tasks:
			if(mRowId!=null){
	        Intent intent = new Intent(this, TaskActivity.class);
	        intent.putExtra(mDbHelper.PROJECTID, mRowId); //projectid
	        startActivityForResult(intent, ACTIVITY_CREATE);		
			}
			else
		   Toast.makeText(ProjectActivity.this, "you must create a project at first", Toast.LENGTH_SHORT).show();					
		break;
		case R.id.btn_update:
			if(TextUtils.isEmpty(et_title.getText())){
				Toast.makeText(ProjectActivity.this, "title can not null", Toast.LENGTH_SHORT).show();
				return ;
			}
			if(TextUtils.isEmpty(et_project_des.getText())){
				Toast.makeText(ProjectActivity.this, "project description can not null", Toast.LENGTH_SHORT).show();
				return ;
			}if(TextUtils.isEmpty(et_course_number.getText())){
				Toast.makeText(ProjectActivity.this, "course number can not null", Toast.LENGTH_SHORT).show();
				return ;
			}if(TextUtils.isEmpty(et_Instructor_Name.getText())){
				Toast.makeText(ProjectActivity.this, "instructor name can not null", Toast.LENGTH_SHORT).show();
				return ;
			}if(TextUtils.isEmpty(et_project_number.getText())){
				Toast.makeText(ProjectActivity.this, "project number can not null", Toast.LENGTH_SHORT).show();
				return ;
			}
			int year1 = et_due_date.getYear();
			String month1 = et_due_date.getMonth()+"";
			if(et_due_date.getMonth()<10)
				month1 ="0"+month1;
			String day1 = et_due_date.getDayOfMonth()+"";
			if(et_due_date.getDayOfMonth()<10)
				day1 ="0"+day1;
			String time1 = year1+""+month1+""+day1+"";
			int priority1 = Integer.valueOf( (String) spinner.getSelectedItem());
			int radioId1 = rg.getCheckedRadioButtonId();
			String status1 = radioId1 == R.id.complete ? "complete" : "incomplete";
			mDbHelper.open();
		
			 boolean result = mDbHelper.updateProject(mRowId,et_title.getText().toString(), et_project_des.getText().toString(), 
					et_course_number.getText().toString(), et_Instructor_Name.getText().toString(),
					et_project_number.getText().toString(), time1,status1,priority1);
			if(result){
			Toast.makeText(ProjectActivity.this, "project update successfully", Toast.LENGTH_SHORT).show();
			}
			else{
		    Toast.makeText(ProjectActivity.this, "project update fail", Toast.LENGTH_SHORT).show();				
			}
			mDbHelper.close();	
			break;
			
		default: 
		break;
		}
		
	}
	
	@Override
	protected void onResume() {
		super.onResume();	
		if (mRowId != null) {
		view_query.setSelection(4);
		mDbHelper.open();
		fillData(mDbHelper.fetchTasksByProjectId(mRowId));
		mDbHelper.close();	
		}
	}

	  @Override
	    public void onCreateContextMenu(ContextMenu menu, View v,
	            ContextMenuInfo menuInfo) {
	        super.onCreateContextMenu(menu, v, menuInfo);
	        menu.add(0, DELETE_ID, 0, "delete task");
	    }

	    @Override
	    public boolean onContextItemSelected(MenuItem item) {
	        switch(item.getItemId()) {
	            case DELETE_ID:
	            	mDbHelper.open();
	                AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
	                mDbHelper.deleteTask(info.id);
	              
	                fillData(mDbHelper.fetchTasksByProjectId(mRowId));
	                mDbHelper.close();
	                return true;
	        }
	        return super.onContextItemSelected(item);
	    }

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
	        Intent i = new Intent(this, TaskActivity.class);
	        i.putExtra(mDbHelper.KEY_ROWID, id); //taskid 
	        i.putExtra(mDbHelper.PROJECTID, mRowId); //projectid
	        startActivityForResult(i, ACTIVITY_EDIT);		
		}
		
		@Override
		protected void onActivityResult(int requestCode, int resultCode, Intent data) {
			// TODO Auto-generated method stub
			super.onActivityResult(requestCode, resultCode, data);
			switch(resultCode){
			case 0:
				view_query.setSelection(4);
				mDbHelper.open();
				fillData(mDbHelper.fetchTasksByProjectId(mRowId));
				mDbHelper.close();
				
				break;
			
			default: break;
				
			}
		}

}
